<?php
namespace PhpExtension\Functions;

/**
 * 再帰的にファイルを検索する
 * @param string $directory ディレクトリパス
 * @return \Travasable
 */
function findFilesRecursive ($directory)
{
    $directoryIterator = new \RecursiveDirectoryIterator($directory);
    $iteratorIterator = new \RecursiveIteratorIterator($directoryIterator);
    foreach ($iteratorIterator as $fileInfo) yield $fileInfo;
}
