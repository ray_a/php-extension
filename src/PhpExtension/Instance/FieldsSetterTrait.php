<?php
/**
 * フィールドsetterトレイト
 */
namespace PhpExtension\Instance;

trait FieldsSetterTrait
{

    /**
     * 未定義メソッド呼び出し
     * @param string $method フィールド名
     * @param mixed[] $args 引数リスト
     * @return self
     * @throws \InvalidArgumentException setter引数が未指定
     * @throws \InvalidArgumentException setter引数が2つ以上
     * @throws \RuntimeException 対応するフィールドが存在しない
     * @throws \RuntimeException アクセス禁止フィールドへのアクセス
     * @throws \RuntimeException setter以外の未定義メソッド呼び出し
     */
    public function __call ($method, $args = [])
    {
        $class = static::class;
        if (preg_match('/^set(.+)$/u', $method, $matches) == 1) {
            $argsCount = count($args);
            if ($argsCount == 0) {
                throw new \InvalidArgumentException(
                    "Setter {$class}::{$method}() requires one argument");
            }
            if ($argsCount > 1) {
                throw new \InvalidArgumentException(
                    "Too many arguments for setter {$class}::{$method}()");
            }
            $field = lcfirst($matches[1]);
            $value = reset($args);
            return \Closure::bind(function ($method, $field, $value) use ($class) {
                $isAccessible = array_key_exists($field, get_object_vars($this))
                    && $this->isAccessibleField($field);
                if ($isAccessible) {
                    $this->{$field} = $value;
                    return $this;
                }
                throw new \RuntimeException(
                    "Undefined method {$class}::{$method}() via __call()");
            }, $this, $class)->__invoke($method, $field, $value);
        }
        /* setXXX() 以外のメソッドを許容しない */
        throw new \RuntimeException("Undefined method {$class}::{$method}() via __call()");
    }

    abstract protected function isAccessibleField ($field);

}
