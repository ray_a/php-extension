<?php
/**
 * 読み取り専用プロパティgetterサポートトレイト
 */
namespace PhpExtension\Instance;

trait ReadOnlyFieldsGetterSupportTrait
{

    use ReadOnlyFieldsTrait {
        ReadOnlyFieldsTrait::__get as baseGet;
    }

    /**
     * 未定義フィールドへのアクセス
     * @param string $name フィールド名
     * @return mixed
     */
    public function __get ($name)
    {
        return \Closure::bind(
            function ($name) {
                /* getterサポート */
                $getter = 'get' . ucfirst($name);
                if (method_exists($this, $getter)) {
                    return $this->{$getter}();
                }
                return $this->baseGet($name);
            }, $this, static::class
        )->__invoke($name);
    }

}
