<?php
/**
 * 読み取り専用プロパティトレイト
 */
namespace PhpExtension\Instance;

trait ReadOnlyFieldsTrait
{

    /** アクセス禁止フィールドプレフィクスリスト @var string[] */
    private $inaccessiblePrefixList = [];

    /**
     * 未定義フィールドへのアクセス
     * @param string $name フィールド名
     * @return mixed
     * @throws \RuntimeException 対応するフィールドが存在しない
     */
    public function __get ($name)
    {
        return \Closure::bind(function ($name) {
            if (array_key_exists($name, get_object_vars($this))) {
                if ($this->isAccessibleField($name)) return $this->{$name};
            }
            /* 存在しないフィールド、アクセス禁止フィールドであれば例外を送出 */
            throw new \RuntimeException("Undefined property via __get(): {$name}");
        }, $this, static::class)->__invoke($name);
    }

    /**
     * フィールドはアクセス可能か
     * @param string $name
     * @return boolean
     */
    protected function isAccessibleField ($name)
    {
        $pattern = implode('|', $this->inaccessiblePrefixList);
        return empty($pattern) || preg_match("/^({$pattern})/u", $name) == 0;
    }

    /**
     * アクセス禁止フィールドプレフィクスリストを設定する
     * @param ...string ...$prefixList プレフィクスリスト
     * @return self
     */
    protected function setInaccessiblePrefixList (...$prefixList)
    {
        $this->inaccessiblePrefixList = [];
        return $this->addInaccessiblePrefix(...$prefixList);
    }

    /**
     * アクセス禁止フィールドプレフィクスリストを追加する
     * @param ...string ...$prefixList プレフィクスリスト
     * @return self
     */
    protected function addInaccessiblePrefix (...$prefixList)
    {
        foreach ($prefixList as $prefix) {
            $this->inaccessiblePrefixList[] = $prefix;
        }
        return $this;
    }

}
