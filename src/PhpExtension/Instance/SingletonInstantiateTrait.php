<?php
/**
 * シングルトンインスタンス生成トレイト
 */
namespace PhpExtension\Instance;

trait SingletonInstantiateTrait
{

    /**
     * シングルトンインスタンスを取得する
     * @param mixed[] ...$args 引数リスト
     * @return self
     */
    public static function createSingletonInstance (...$args)
    {
        static $instance = null;
        return \Closure::bind(function () use (&$instance) {
            if (is_null($instance)) {
                $instance = new static(...$args);
            }
            return $instance;
        }, null, static::class)->__invoke();
    }

}
