<?php
/**
 * 静的ファクトリメソッドトレイト
 */
namespace PhpExtension\Instance;

trait StaticInstantiateTrait
{

    /**
     * インスタンスを取得する
     * @param mixed[] ...$args 引数リスト
     * @return self
     */
    public static function createInstance (...$args)
    {
        return \Closure::bind(
            function (...$args) {
                return new static(...$args);
            }, null, static::class
        )->__invoke(...$args);
    }

}
