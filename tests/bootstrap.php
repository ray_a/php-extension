<?php
session_start();

$closure = function () {
    $projectRoot = realpath(__dir__ . '/..') . '/';
    require "{$projectRoot}vendor/autoload.php";

    $cacheDir = '/tmp/aspect_mock/';
    if (!is_dir($cacheDir)) mkdir($cacheDir, 0777, true);
    \AspectMock\Kernel::getInstance()->init([
        'appDir' => $projectRoot,
        'cacheDir' => $cacheDir,
        'includePaths' => [
            "{$projectRoot}src/PhpExtension",
        ],
    ]);
};

$closure->__invoke();
