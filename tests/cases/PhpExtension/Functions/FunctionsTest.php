<?php
/**
 * ファイルシステムファインダ ユニットテスト
 */
namespace Tests\Cases\PhpExtension\Functions;

use PhpExtension\Functions;
use org\bovigo\vfs;

class FunctionsTest
    extends \Tests\Cases\TestAbstract
{

    public function test_findFilesRecursive ()
    {
        /* Arrange */
        $tree = [
            'root/sub1a/file_1a_1.md',
            'root/sub1a/sub2a/file_1a2a_1.md',
            'root/sub1a/sub2a/file_1a2a_2.md',
            'root/sub1a/sub2b/file_1a2b_1.md',
            'root/sub1b/sub2a/file_1b2a_1.md',
            'root/sub1b/sub2c/',
        ];
        foreach ($tree as $path) {
            $isDir = preg_match('/\/$/u', $path) == 1;
            $dirPath = vfs\vfsStream::url(($isDir ? $path : dirname($path)));
            if (!is_dir($dirPath)) mkdir($dirPath, 0777, true);
            if (!$isDir) file_put_contents(vfs\vfsStream::url($path), 'test');
        }
        $directory = vfs\vfsStream::url('root');
        /* Act */
        /* Assert */
        foreach (Functions\findFilesRecursive($directory) as $fileInfo) {
            $this->assertContains($fileInfo->getPathname(), [
                'vfs://root/.',
                'vfs://root/..',
                'vfs://root/sub1a/.',
                'vfs://root/sub1a/..',
                'vfs://root/sub1a/file_1a_1.md',
                'vfs://root/sub1a/sub2a/.',
                'vfs://root/sub1a/sub2a/..',
                'vfs://root/sub1a/sub2a/file_1a2a_1.md',
                'vfs://root/sub1a/sub2a/file_1a2a_2.md',
                'vfs://root/sub1a/sub2b/.',
                'vfs://root/sub1a/sub2b/..',
                'vfs://root/sub1a/sub2b/file_1a2b_1.md',
                'vfs://root/sub1b/.',
                'vfs://root/sub1b/..',
                'vfs://root/sub1b/sub2a/.',
                'vfs://root/sub1b/sub2a/..',
                'vfs://root/sub1b/sub2a/file_1b2a_1.md',
                'vfs://root/sub1b/sub2c/.',
                'vfs://root/sub1b/sub2c/..',
            ]);
        }
    }

}

