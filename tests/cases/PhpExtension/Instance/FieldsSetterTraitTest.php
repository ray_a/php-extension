<?php
/**
 * フィールドsetterトレイト ユニットテスト
 */
namespace Tests\Cases\PhpExtension\Instance;

use Tests\Cases\PhpExtension\Instance\FieldsSetterTrait\TestObject;

/**
 * @coversDefaultClass \PhpExtension\Instance\FieldsSetterTrait
 */
final class FieldsSetterTraitTest
    extends \Tests\Cases\TestAbstract
{

    /**
     * @group __call
     * @covers ::__call
     */
    public function test_call ()
    {
        /* Arrange */
        $id = 3;
        $name = 'test name';
        $description = 'test description';
        /* Act */
        $to = TestObject::createInstance()
            ->setId($id)
            ->setName($name)
            ->setDescription($description);
        /* Assert */
        $this->assertEquals($id, $to->id);
        $this->assertEquals($name, $to->name);
        $this->assertEquals($description, $to->description);
    }

    public function test_setFailureWithNoArguments ()
    {
        /* Arrange */
        $class = TestObject::class;
        /* Act */
        /* Assert */
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Setter {$class}::setName() requires one argument");
        TestObject::createInstance()->setName();
    }

    public function test_setFailureWithMoreThanTwoArguments ()
    {
        /* Arrange */
        $class = TestObject::class;
        /* Act */
        /* Assert */
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Too many arguments for setter {$class}::setName()");
        TestObject::createInstance()->setName('last', 'first');
    }

    public function test_setFailureOnNonExistingField ()
    {
        /* Arrange */
        $class = TestObject::class;
        /* Act */
        /* Assert */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            "Undefined method {$class}::setDateOfBirth() via __call()");
        TestObject::createInstance()->setDateOfBirth('1980-04-02');
    }

    public function test_setFailureOnInAccessibleField ()
    {
        /* Arrange */
        $class = TestObject::class;
        /* Act */
        /* Assert */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            "Undefined method {$class}::set_secret() via __call()");
        TestObject::createInstance()->set_secret('secret');
    }

    public function test_callFailureOnNonSetterMethod ()
    {
        /* Arrange */
        $class = TestObject::class;
        /* Act */
        /* Assert */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(
            "Undefined method {$class}::changeName() via __call()");
        TestObject::createInstance()->changeName('other name');
    }

}


namespace Tests\Cases\PhpExtension\Instance\FieldsSetterTrait;

class TestObject
{
    use
        \PhpExtension\Instance\StaticInstantiateTrait,
        \PhpExtension\Instance\ReadOnlyFieldsTrait,
        \PhpExtension\Instance\FieldsSetterTrait;

    private $_secret;
    private $id;
    private $name;
    private $description;

    public function __construct ()
    {
        $this->setInaccessiblePrefixList('_');
    }

}
