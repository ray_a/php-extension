<?php
/**
 * 読み取り専用プロパティgetterサポートトレイト ユニットテスト
 */
namespace Tests\Cases\PhpExtension\Instance;

use Cake\Chronos\Chronos;
use Tests\Cases\PhpExtension\Instance\ReadOnlyFieldsGetterSupportTrait\TestClass;

/**
 * @coversDefaultClass \PhpExtension\Instance\ReadOnlyFieldsGetterSupportTrait
 */
final class ReadOnlyFieldsGetterSupportTraitTest
    extends \Tests\Cases\TestAbstract
{

    /**
     * @group __get
     * @covers ::__get
     */
    public function testGet ()
    {
        /* Arrange */
        $id = 4;
        $lastName = '姓';
        $firstName = '名';
        $dateOfBirth = '1980-11-23';
        Chronos::setTestNow(
            Chronos::createFromFormat('Y-m-d H:i:s', '2018-05-21 00:00:00'));
        $test = TestClass::createInstance($id, $lastName, $firstName, $dateOfBirth);
        /* Act */
        /* Assert */
        $this->assertEquals($id, $test->id);
        $this->assertEquals($lastName, $test->lastName);
        $this->assertEquals($firstName, $test->firstName);
        $this->assertEquals('姓 名', $test->fullName);
        $this->assertEquals(37, $test->age);
    }

}

namespace Tests\Cases\PhpExtension\Instance\ReadOnlyFieldsGetterSupportTrait;

use Cake\Chronos\Chronos;

class TestClass
{
    use
        \PhpExtension\Instance\StaticInstantiateTrait,
        \PhpExtension\Instance\ReadOnlyFieldsGetterSupportTrait;

    private $id;
    private $lastName;
    private $firstName;
    private $dateOfBirth;

    public function __construct ($id, $lastName, $firstName, $dateOfBirth)
    {
        $this->id = $id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->dateOfBirth =$dateOfBirth;
    }

    public function getFullName ()
    {
        return "{$this->lastName} {$this->firstName}";
    }

    private function getAge ()
    {
        $dateOfBirth = Chronos
            ::createFromFormat('Y-m-d h:i:s', "{$this->dateOfBirth} 00:00:00");
        return $dateOfBirth->diff(Chronos::now())->y;
    }

}
