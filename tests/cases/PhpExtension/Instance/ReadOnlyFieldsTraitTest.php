<?php
/**
 * 読み取り専用プロパティトレイト ユニットテスト
 */
namespace Tests\Cases\PhpExtension\Instance;

use Tests\Cases\PhpExtension\Instance\ReadOnlyFieldsTrait\TestClass;

/**
 * @coversDefaultClass \PhpExtension\Instance\ReadOnlyFieldsTrait
 */
final class ReadOnlyFieldsTraitTest
    extends \Tests\Cases\TestAbstract
{

    /**
     * @group __get
     * @covers ::__get
     */
    public function testGet ()
    {
        /* Arrange */
        $id = 3;
        $name = 'test object name';
        $test = TestClass::createInstance($id, $name);
        /* Act */
        /* Assert */
        $this->assertEquals($id, $test->id);
        $this->assertEquals($name, $test->name);
    }

    /**
     * @group __get
     * @covers ::__get
     */
    public function testGetFailureOnNonExistingField ()
    {
        /* Arrange */
        $id = 3;
        $name = 'test object name';
        $test = TestClass::createInstance($id, $name);
        /* Act */
        /* Assert */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Undefined property via __get(): createdAt');
        $createdAt = $test->createdAt;
    }

    /**
     * @group __get
     * @covers ::__get
     */
    public function testGetFailureOnInAccessibleField ()
    {
        /* Arrange */
        $id = 3;
        $name = 'test object name';
        $test = TestClass::createInstance($id, $name);
        /* Act */
        /* Assert */
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Undefined property via __get(): _secret');
        $secret = $test->_secret;
    }

}

namespace Tests\Cases\PhpExtension\Instance\ReadOnlyFieldsTrait;

class TestClass
{

    use
        \PhpExtension\Instance\StaticInstantiateTrait,
        \PhpExtension\Instance\ReadOnlyFieldsTrait;

    private $_secret;
    private $id;
    private $name;

    public function __construct ($id, $name)
    {
        $this->setInaccessiblePrefixList('_');
        $this->id = $id;
        $this->name = $name;
    }

}
