<?php
/**
 * シングルトンインスタンス生成トレイト ユニットテスト
 */
namespace Tests\Cases\PhpExtension\Instance;

use Tests\Cases\PhpExtension\Instance\SingletonInstantiateTrait\TestObject;

/**
 * @coversDefaultClass \PhpExtension\Instance\SingletonInstantiateTrait
 */
final class SingletonInstantiateTraitTest
    extends \Tests\Cases\TestAbstract
{

    /**
     * @group createSingletonInstance
     * @covers ::createSingletonInstance
     */
    public function test_instantiate ()
    {
        /* Arrange */
        /* Act */
        $one = TestObject::createSingletonInstance();
        $two = TestObject::createSingletonInstance();
        /* Assert */
        $this->assertSame($one, $two);
    }

}

namespace Tests\Cases\PhpExtension\Instance\SingletonInstantiateTrait;

class TestObject
{

    use
        \PhpExtension\Instance\SingletonInstantiateTrait;

}
