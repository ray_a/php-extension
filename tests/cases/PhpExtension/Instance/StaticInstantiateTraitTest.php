<?php
/**
 * 静的ファクトリメソッドトレイト
 */
namespace Tests\Cases\PhpExtension\Instance;

use Tests\Cases\PhpExtension\Instance\StaticInstantiateTrait\TestClass;

/**
 * @coversDefaultClass \PhpExtension\Instance\StaticInstantiateTrait
 */
final class StaticInstantiateTrait
    extends \Tests\Cases\TestAbstract
{

    /**
     * @group createInstance
     * @covers ::createInstance
     */
    public function testInstance ()
    {

        /* Arrange */
        $id = 'test_id';
        $name = 'test_name';
        /* Act */
        $testClass = TestClass::createInstance($id, $name);
        /* Assert */
        $this->assertEquals($id, $testClass->id);
        $this->assertEquals($name, $testClass->name);
    }

}

namespace Tests\Cases\PhpExtension\Instance\StaticInstantiateTrait;

use PhpExtension\Instance;

class TestClass
{

    use
        Instance\StaticInstantiateTrait,
        Instance\ReadOnlyFieldsTrait;

    private $id;

    private $name;

    private function __construct ($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}
