<?php
/**
 * 抽象ユニットテスト
 */
namespace Tests\Cases;

use org\bovigo\vfs;
use Cake\Chronos\Chronos;

abstract class TestAbstract
    extends \PHPUnit\Framework\TestCase
{

    /** プロジェクトルートディレクトリ @var string */
    private $projectRoot;

    /**
     * @before
     */
    public function baseSetUp ()
    {
        $this->projectRoot = realpath(__dir__ . '/../../') . '/';
        vfs\vfsStream::setup();
        Chronos::setTestNow(null);
    }

    /**
     * @after
     */
    public function baseTearDown ()
    {
        \AspectMock\Test::clean();
    }

    /**
     * 対象インスタンスのメソッドを公開、非公開を問わず実行する
     * @param object $object 対象インスタンス
     * @param string $methodName メソッド名
     * @param ...mixed ...$args 引数
     * @return mixed
     */
    protected function invoke ($object, $methodName, ...$args)
    {
        return \Closure::bind(
            function ($methodName, ...$args) {
                return $this->{$methodName}(...$args);
            }, $object, get_class($object)
        )->__invoke($methodName, ...$args);
    }

    /**
     * 対象インスタンスのフィールドを公開、非公開問わず参照する
     * @param object $object 対象インスタンス
     * @param string $fieldName フィールド名
     * @return mixed
     */
    protected function getField ($object, $fieldName)
    {
        return \Closure::bind(
            function ($fieldName) {
                return $this->{$fieldName};
            }, $object, get_class($object)
        )->__invoke($fieldName);
    }

}
